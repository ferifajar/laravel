<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class DashboardController extends Controller
{
    public function index() 
    {
        $users = User::all();
 
        $user1 = $users->find(1);
        $user2 = $users->find(2);


        return view('dashboard',[
            'title' => 'Dashboard',
            'user1' => $user1,
            'user2' => $user2,
            'tabungan1' => $user1->saves->first(),
            'tabungan2' => $user2->saves->first(),
        ]);
    }
}
