<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Models\Save;
use App\Models\History;
use Illuminate\Support\Facades\Input;

use Session;

class HistoryController extends Controller
{


    public function index(Save $save) {

        return view('history', [
            'title' => 'History',
            'history' => $save->histories, 
        ]);
    }

    public function create(Save $save) {

        return view('nabung-form',[
            'title' => 'Nabung',    
            'save' => $save->id
        ]);
    }

    public function tarik(Save $save) {
        return view('tarik-form',[
            'title' => 'Ambil',    
            'save' => $save->id
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'total' => ['required'],
        ],
        [
            'total.required' => 'Jangan lupa di isi mau nabung berapa ok'
        ]
    
    
    );

        $save_id = $request->save_id;
        $total = str_replace(".", "", $request->total);

        $history = new History;
        $history->save_id = $save_id;
        $history->total = $total;
        $history->type = 'Tabungan Masuk';
        $history->description = $request->description;

        $history->save();

        $save = Save::find($request->save_id);
        $save->total = $save->total + $total;
        $save->save();


        Session::flash('message', 'Tabungan berhasil ditambah, semangat nabungnya !! ');
        return redirect()->route('tabungan',auth()->user()->id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function TarikStore(Request $request)
    {

        $save = Save::find($request->save_id);

        $validatedData = $request->validate([
            'total' => [
                'required',
                'lt:'.$save->total
            ],
        ],
        [
            'total.required' => 'Jangan lupa di isi mau nabung berapa ok',
            'total.lt' => 'Gabisa ngambil '.$request->total.' dong, kan tabungan nya cuma '. number_format($save->total,0,',','.').' dikit aja ya ngambilnya',
            
        ]
    );
   
    $save_id = $request->save_id;
    $total = str_replace(".", "", $request->total);

        $history = new History;
        $history->save_id = $save_id;
        $history->total = $total;
        $history->type = 'Tabungan Diambil';
        $history->description = $request->description;

        $history->save();

        $save = Save::find($request->save_id);
        $save->total = $save->total - $total;
        $save->save();


        Session::flash('message', 'Tabungan berhasil diambil, udah cukup ya ngambil tabungan nya, diambil terus abis kali !! ');
    
        return redirect()->route('tabungan', auth()->user()->id);
    }
}
