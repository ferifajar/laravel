<?php

namespace App\Models;

use App\Models\Save;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class History extends Model
{
    use HasFactory;

    protected $guarged = ['id'];

    public function saves() {
        return $this->belongsTo(Save::class);
    }
}
