<?php

namespace App\Models;

use App\Models\History;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Save extends Model
{
    use HasFactory;

    protected $guarded =  ['id'];

    public function histories() {
        return $this->hasMany(History::class)->latest();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
