@extends('layouts.app')

@section('content')
@include('layouts.headers.header-gradient')



<div class="container-fluid mt--7">
    <div class="col">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                        <h2 class=" mb-0">Tabungan</h2>
                    </div>
                    
                </div>
                <div class="card-body">
                    @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>    
          <strong>{{ $message }}</strong>
      </div>
    @endif
                    <div class="row">
                        @foreach ( $save as $s )
                            
                       
                        <div class="col-xl-3 col-lg-6 mr-7">
                            <div style="width: 18rem;">
                                <div class="card card-stats">
                                    <!-- Card body -->
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col">
                                                <h5 class="card-title text-uppercase text-muted mb-0">{{$s->name}}
                                                </h5>
                                                <span class="h2 font-weight-bold mb-0">@currency($s->total)</span>
                                            </div>
                                                <div class="col-auto">
                                                <div class="icon icon-shape bg-orange text-white rounded-circle shadow">
                                                    <i class="ni ni-chart-pie-35"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mt-3 mb-0 text-sm">
                                            <span class="text-nowrap mr-2"><i class="fa fa-arrow-up"></i>Target </span>
                                            <span class="text-nowrap"> @currency($s->target)</span>
                                        </p>
                                        <div class="progress-wrapper">
                                            <div class="progress-info">
                                                <div class="progress-label">
                                                    <span>Target</span>
                                                </div>
                                                <div class="progress-percentage">
                                                    <span>{{ $percent = ($s->total / $s->target) * 100 }}%</span>
                                                </div>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="60"
                                                    aria-valuemin="0" aria-valuemax="100" style="width: {{$percent}}%;"></div>
                                            </div>

                                            <a href="/nabung/{{ $s->id }}" class="btn btn-primary btn-sm">Nabung</a>
                                            <a href="/tarik/{{ $s->id }}" class="btn btn-danger btn-sm">Ambil</a>
                                            <a href="/history/{{ $s->id }}" class="btn btn-secondary btn-sm">History</a>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        @endforeach                     
                    </div>
                </div>


            </div>

        </div>



        @include('layouts.footers.auth')
    </div>
    @endsection