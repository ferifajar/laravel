@extends('layouts.app')

@section('content')
@include('layouts.headers.header-gradient')



<div class="container-fluid mt--7">
    <div class="col">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                        <h2 class=" mb-0">Nabung</h2>
                    </div>
                    <div class="col">
                        <ul class="nav nav-pills justify-content-end">
                            <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales"
                                data-update="{&quot;data&quot;:{&quot;datasets&quot;:[{&quot;data&quot;:[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}"
                                data-prefix="$" data-suffix="k">
                                <a href="/tabungan/{{auth()->user()->id}}" class="nav-link py-2 px-3 active">
                                    <span class="d-none d-md-block"><i class="ni ni-bold-left"></i> Kembali</span>
                                    <span class="d-md-none"><i class="ni ni-bold-left"></i></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/nabung-post" method="POST">
                        @csrf
                        <input type="hidden" name="save_id" value="{{ $save }}">
                        <div class="form-group">
                            <label for="total" class="form-control-label">Jumlah Nabung</label>
                            <input class="form-control  @error('total') is-invalid @enderror" type="text" name="total" id="total" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
                            @error('total')
	                        <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="description" class="form-control-label">Descrciption</label>
                            <input class="form-control  @error('description') is-invalid @enderror" type="text" name="description" id="description">

                            @error('description')
	                        <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="Submit" class="btn btn-primary">Submit</button>
                        </div>

                        
                    </form>
                </div>


            </div>

        </div>



        @include('layouts.footers.auth')
    </div>
    @endsection