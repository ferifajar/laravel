@extends('layouts.app')

@section('content')
@include('layouts.headers.header-gradient')



<div class="container-fluid mt--7">
    <div class="col">
        <div class="card">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                        <h2 class=" mb-0">History</h2>
                    </div>
                    <div class="col">
                        <ul class="nav nav-pills justify-content-end">
                            <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales"
                                data-update="{&quot;data&quot;:{&quot;datasets&quot;:[{&quot;data&quot;:[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}"
                                data-prefix="$" data-suffix="k">
                                <a href="/tabungan/{{auth()->user()->id}}" class="nav-link py-2 px-3 active">
                                    <span class="d-none d-md-block"><i class="ni ni-bold-left"></i> Kembali</span>
                                    <span class="d-md-none"><i class="ni ni-bold-left"></i></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            <div class="card-body">
                    @forelse ($history as $h)
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="card-title text-uppercase text-muted mb-0">{{$h->type}}</h5>
                                    <small class="text-muted">{{ $h->created_at->format('j F, Y') }} | {{ $h->created_at->format('H:i:s') }}</small>
                                    <small class="text-muted">{{ $h->description }}</small>
                                    
                                </div>
                                <div class="col-6">
                                    @if ($h->type == 'Tabungan Masuk')
                                    <h4 class="text-success mr-2 float-right"><i class="ni ni-fat-add"></i> @currency($h->total)</h4>
                                    @else
                                    <h4 class="text-danger mr-2 float-right"><i class="ni ni-fat-delete"></i> @currency($h->total)</h4>
                                    @endif
                                
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    @empty
                    <div class="alert alert-warning" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text">Historynya masih kosong nih yuk nabung!</span>
                    </div>
                    @endforelse

               


            </div>




        </div>
    </div>



        @include('layouts.footers.auth')
    </div>
    @endsection