<?php

namespace Database\Seeders;

use App\Models\Save;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::Create([
            'first_name' => 'Feri',
            'last_name' => 'Fajar',
            'email' => 'ferifajar30@gmail.com',
            'password' => Hash::make('secret'),
        ]);
        User::Create([
            'first_name' => 'Wina',
            'last_name' => 'Rahayu',
            'email' => 'rahayuwina24@gmail.com',
            'password' => Hash::make('secret'),
        ]);

        Save::Create([
            'name' => 'Tabungan Utama',
            'user_id' => '1',
            'target' => '10000000',
            'total' => '0', 
        ]);
        Save::Create([
            'name' => 'Tabungan Utama',
            'user_id' => '2',
            'target' => '10000000',
            'total' => '0',
        ]);
    }
}
