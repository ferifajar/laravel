<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaveController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->middleware('auth');
Route::get('/login', [LoginController::class, 'index'])->middleware('guest')->name('login');
Route::get('/nabung/{save}',  [HistoryController::class, 'create'])->middleware('auth');
Route::get('/tarik/{save}',  [HistoryController::class, 'tarik'])->middleware('auth');
Route::get('/history/{save}',  [HistoryController::class, 'index'])->name('history')->middleware('auth');
Route::get('/tabungan/{user}',  [SaveController::class, 'index'])->name('tabungan')->middleware('auth');

Route::post('/nabung-post',  [HistoryController::class, 'store'])->name('nabung')->middleware('auth');
Route::post('/tarik-post',  [HistoryController::class, 'TarikStore'])->name('tarik')->middleware('auth');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);